function factorial(n){
	if(typeof n !== "number") return undefined; // to check if values is number
	if(n < 0 ) return undefined; // to check if the value should be more than 0
	if(n === 0) return 1; // to check if value is 0 return 1
	if(n === 1) return 1; // to check if value is 1 return 1
	return n * factorial(n-1);
};

module.exports = {
	factorial: factorial
}