const { factorial } = require("../src/util.js");

const { expect, assert } = require("chai");


describe("test_fun_factorials", () => {
	it("test_fun_factorial_0!_is_120", () => {
	const product = factorial(5);
	expect(product).to.equal(120);
	});

	it("test_fun_factorial_1!_is_1", () => {
	const product = factorial(1);
	expect(product).to.equal(1);
	});

	//test for negative numbers
	it("test_fun_factorials_neg1_is_undefined", () => {
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	})


		//test for negative numbers
	it("test_fun_factorials_invalid_number_is_undefined", () => {
		const product = factorial ("25");
		expect(product).to.equal(undefined);
	})




});